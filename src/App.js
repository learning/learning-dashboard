import * as React from 'react'
import './App.css'
// import Button from 'antd/lib/button'
import { DashboardPie } from './components/DashboardPie'

class App extends React.Component {

  constructor(props) {
    super(props)

    this._async = this._async.bind(this)

    this.state = {
      data: []
    }
  }

  componentWillMount() {
    this._async()
  }

  _async = () => {
    fetch('../data/pie')
      .then((response) => {
        return response.json()
      })
      .then((response) => {
        this.setState({ data: response })
      })
  }

  render() {
    return (
      <DashboardPie
        header="Interaction by Project"
        data={this.state.data}
      />
    )
  }
}

export default App
